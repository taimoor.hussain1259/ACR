#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 20 11:51:35 2018

@author: taimoor
"""

import sys
import os

def getFileName(fn):
    rvr = fn[::-1]
    filename = ''
    flag = 0
    for s in rvr:
        if s == '/' and s != '.':
            break
        
        if flag == 1:
            filename += s
            
        if s == '.':
            flag = 1
            
    return filename[::-1]
    
def sequenceFinder(genome, seqs, startrange, endrange):
    for seq in seqs:
        ngram = len(seq)
            
        for i in range(startrange-1, endrange - ngram + 1):
            if genome[i:i+ngram] == seq:
                genome = genome[:i]+ ('N' * ngram) +genome[i+ngram:]
    
    return genome

#################################################################################################################################################################################
################################################################To Parse the Crispr Array File###################################################################################
#################################################################################################################################################################################

def crisprParser(genomefile, crisprtablefile):
    genomeread = open(genomefile, 'r')
    crisprread = open(crisprtablefile, 'r')
    
    if crisprread.read().find('No CRISPR elements were found.') > 0:
        print 'No CRISPR elements were found.'      
        genomeread.close()
        
        firstLine = ''
        with open(genomefile, 'r') as f:
            for l in f:
                if '>' in l:
                    firstLine = l
                    break
        return firstLine[firstLine.find('>')+1:], '', [], []#firstLine[firstLine.find('>')+1:], firstLine+genome, foundSpacers, foundLocations

    crisprread.seek(0)
    for i in range(2):
        print crisprread.readline()

    
    genome = ''
    firstLine = ''
    with open(genomefile, 'r') as f:
        for l in f:
            if '>' not in l:
                genome += l
            else:
                firstLine = l
            
    genome = genome.replace('\n', '')
    genome = genome.replace(' ', '')
    foundSpacers = []
    foundLocations = []
    try:
        while True:
            r = crisprread.readline()
            loc1 = 0
            while '---' not in r:
                if 'Time to find repeats:' in r:
                    1/0
                    
                if 'Range' in r:
                    loc1 = crisprread.tell() - len(r) 
                r = crisprread.readline()
            loc2 = crisprread.tell()
            crisprread.seek(loc1)
            r = crisprread.readline().split(' ')
            startrange = int(r[5])
            endrange = int(r[7])
            crisprread.seek(loc2)
            r = crisprread.readline()
            spacers = []
            print 'Start Range: ', startrange, '\tEndRange : ', endrange
            while len(r.split('\t')) > 4:
                print r.split('\t')[0], ':', r.split('\t')[3]
                spacers.append(r.split('\t')[3])
                foundSpacers.append(r.split('\t')[3])
                foundLocations.append(r.split('\t')[0])
                r = crisprread.readline()
                
            genome = sequenceFinder(genome, spacers, startrange, endrange)
            
            r = crisprread.readline()
            while '---' not in r:
                if 'Time to find repeats:' in r:
                    1/0
                continue
    except:
        print 'Finished Spacers Checking Proceedure'
        
    genomeread.close()
    crisprread.close()
    return firstLine[firstLine.find('>')+1:], firstLine+genome, foundSpacers, foundLocations


def samparser(fn):    
    sam = ''
    with open(fn) as o:
        sam = o.read()
        
    listsam = sam.split('\n')
    foundMatches = []
    foundMatchLoc = []
    atPG = 0
    if len(listsam) > 2:
        for i in range(len(listsam)-1):
            if atPG == 1:
                samcolumns = listsam[i].split('\t')
                if '*' not in samcolumns[2]:
		            foundMatches.append(samcolumns[9])
		            foundMatchLoc.append(samcolumns[3])
            
            if '@PG' in listsam[i]:
				atPG = 1

    return foundMatchLoc, foundMatches

def similarityMeasure(match, query):
    z=zip(match, query)
    matchcount = 0
    for i,j in z:
        if i==j:
            matchcount += 1
            
    return str(matchcount), (float(matchcount)/len(query))*100

#################################################################################################################################################################################
######################################################################Self Targeting Findings####################################################################################
#################################################################################################################################################################################

def selfTargetFunc(fn, crisprTableFile, processingfolder, crisprtabledestination, samdestination, csvdestination, emptytempfolder, scorelimit):
    cfn = getFileName(fn)
    print cfn+'.fna is processing for Self Targeting'
    
    
    title, genome, spacers, locations = crisprParser(fn, crisprTableFile)
    
    if len(spacers) > 0:
        spacersFind = ''
        for i in range(len(spacers)):
            spacersFind += '> Spacer' + str(i) + '\n'
            spacersFind += spacers[i] + '\n'
        
        genomeLoc = processingfolder+'genome.fa'
        spacerLoc = processingfolder+'spacer.fa'
        genomefile = open(genomeLoc, 'w')
        spacerfile = open(spacerLoc, 'w')
        
        genomefile.write(unicode(genome))
        spacerfile.write(unicode(spacersFind))
        
        genomefile.close()
        spacerfile.close()
        
        os.system('bwa index ' + genomeLoc)
        os.system('bwa aln -n 4 -o 0 -k 4 -N ' + genomeLoc + ' ' + spacerLoc +' > ' + processingfolder + 'output.sai')
        os.system('bwa samse ' + genomeLoc + ' ' + processingfolder + 'output.sai ' + spacerLoc + ' > ' + processingfolder + cfn + '.sam')
        
        samData = ''
        with open(processingfolder + cfn + '.sam', 'r') as read:
			samData += title
			for l in read:
				samData += l
                
        with open(processingfolder + cfn + '.sam', 'w') as write:
			write.write(unicode(samData))

        os.system('mv -f ' + crisprTableFile + ' ' + crisprtabledestination)
        os.system('mv -f ' + processingfolder + cfn + '.sam'  + ' ' + samdestination)
        ####################################################################################################################################
        writeFileCode = ''
        for i in range(len(spacers)):
            spacersFind = ''
            spacersFind += '> Spacer' + str(i) + '\n' + spacers[i]            
            
            spacerfile = open(spacerLoc, 'w')
            spacerfile.write(unicode(spacersFind))
            spacerfile.close()
            
            #os.system('bwa index ' + genomeLoc)
            os.system('bwa aln -n 4 -o 0 -k 4 -N ' + genomeLoc + ' ' + spacerLoc +' > ' + processingfolder + 'output.sai')
            os.system('bwa samse ' + genomeLoc + ' ' + processingfolder + 'output.sai ' + spacerLoc + ' > ' + processingfolder + cfn + '.sam')
            
            foundMatchLoc, foundMatches = samparser(processingfolder + cfn + '.sam')
            if len(foundMatches) > 0:            
                for j in range(len(foundMatches)):
                    matchcount, score = similarityMeasure(foundMatches[j], spacers[i])
                    if float(scorelimit) <= score:
                        writeFileCode += unicode(locations[i]+'\t'+str(len(spacers[i]))+'\t'+spacers[i]+'\t'+foundMatchLoc[j]+'\t'+str(len(foundMatches[j]))+'\t'+foundMatches[j]+'\t'+matchcount+'\t'+ str(score) + '\n')
                
        if len(writeFileCode) > 0:
            if not os.path.isfile(processingfolder + cfn + '.csv'):
                with open(processingfolder + cfn + '.csv', 'w') as o:
                    o.write(unicode(title))
                    o.write(unicode('Spacer_Location\tSpacer_Length\tSpacer\tSelf_Targeting_Location\tSelft_Target_Length\tSelf_Target\tMatches\tSimilarity[%age]\n'))
                    o.write(writeFileCode)
                    
            os.system('mv -f ' + processingfolder + cfn + '.csv'  + ' ' + csvdestination)
        

	if emptytempfolder == 1:
		os.system('rm -rf ' + processingfolder + '*')

    print '######################################################################################'

#################################################################################################################################################################################
######################################################################Crispr Array File creation#################################################################################
#################################################################################################################################################################################

def crisprFunc(fn, jarFileLocation, processingfolder):
    """
    This function takes the genome file and produces crisper table on output 
    """    
    filename = getFileName(fn)
    title = ''
    genome = ''
    
    with open(fn,'r') as read:
        for l in read:
            if '>' in l:
                title=l
            else:
                l = l.replace(' ', '')
                l = l.replace('\n', '')
                genome += l
   
    with open(fn, 'w') as write:
        write.write(unicode(title+genome))    

    basic_cmd = 'java -cp ' + jarFileLocation + ' crt'
    cmd = basic_cmd + ' \"'+ fn + '\" \"'+processingfolder+ filename + '.tbl\"'    
    os.system(cmd)  
    return processingfolder + filename + '.tbl'

#################################################################################################################################################################################
#########################################################################Start of the Program####################################################################################
#################################################################################################################################################################################
    
if __name__ == "__main__":
    if len(sys.argv) < 6:
        print 'Sorry! Arguments are less. These should be 6.'
        
    fn = sys.argv[1] #genome file path + name
    processingfolder = sys.argv[2] #processing folder
    if processingfolder[len(processingfolder)-1] != '/':
        processingfolder += '/'
        
    jarFileLocation = sys.argv[3] #jar file location+file complete path
        
    crisprtabledestination = sys.argv[4] #where to keep the crispr array file
    if crisprtabledestination[len(crisprtabledestination) - 1] != '/':
        crisprtabledestination += '/'
    
    samdestination = sys.argv[5] #where to keep the sam file
    if samdestination[len(samdestination) - 1] != '/':
        samdestination += '/'
        
    csvdestination = sys.argv[6] #Where to keep the crispr+sam.csv should be kept on output
    if csvdestination[len(csvdestination) - 1] != '/':
		csvdestination += '/'
        
    emptytempfolder = sys.argv[7] #to empty the folder 0: if not empty, 1: if empty
    
    scorelimit = sys.argv[8] #score limit
        
    crisprtableFile = crisprFunc(fn, jarFileLocation, processingfolder)
    selfTargetFunc(fn, crisprtableFile, processingfolder, crisprtabledestination, samdestination, csvdestination, int(emptytempfolder), int(scorelimit))
    
